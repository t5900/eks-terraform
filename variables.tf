variable "region" {
  default = "us-east-1"
}

variable "app_name" {
  default = "django-twitter"
}

variable "app_port" {
  default = 8000
}

variable "cidr_block" {
  type        = string
  default     = "10.0.0.0/16"
  description = "(optional) describe your variable"
}

variable "docker_image" {
  default = "249736386209.dkr.ecr.us-east-1.amazonaws.com/twitter-new:latest"
}

variable "name" {
  default = "irsa-bryitest"
}