terraform init 
terraform fmt
terraform validate
terraform apply --auto-approve

cd route53*
terraform init 

LB_ARN=$(aws elbv2 describe-load-balancers \
--query 'LoadBalancers[0].LoadBalancerArn' | tr -d '"')

terraform import aws_lb.nlb $LB_ARN

# LISTENERS_NUMBER=$(aws elbv2 describe-listeners \
# --load-balancer-arn $LB_ARN \--query 'Listeners[] | length(@)')

# for i in 0 $(expr $LISTENERS_NUMBER - 1)
# do
#     query=$(aws elbv2 describe-listeners \
#     --load-balancer-arn $LB_ARN --query 'Listeners['$i'].Port')
    
#     if [ "$query" = '443' ];
#     then 
#         terraform import aws_lb_listener.nlb_listener_https \
#         $(aws elbv2 describe-listeners --load-balancer-arn $LB_ARN \
#         --query 'Listeners['$i'].ListenerArn' | tr -d '"')

#         terraform import aws_lb_target_group.nlb_tg_https \
#         $(aws elbv2 describe-listeners --load-balancer-arn $LB_ARN \
#         --query 'Listeners['$i'].DefaultActions[0].TargetGroupArn' | tr -d '"')
#     else
#         terraform import aws_lb_listener.nlb_listener \
#         $(aws elbv2 describe-listeners --load-balancer-arn $LB_ARN \
#         --query 'Listeners['$i'].ListenerArn' | tr -d '"')
        
#         terraform import aws_lb_target_group.nlb_tg \
#         $(aws elbv2 describe-listeners --load-balancer-arn $LB_ARN \
#         --query 'Listeners['$i'].DefaultActions[0].TargetGroupArn' | tr -d '"')
#     fi
# done

terraform fmt
terraform validate

terraform apply --auto-approve