data "aws_iam_policy" "AmazonEKSVPCResourceController" {
  arn = "arn:aws:iam::aws:policy/AmazonEKSVPCResourceController"
}

resource "aws_iam_role_policy_attachment" "cni-cluster-role-attachment" {
  role       = module.eks.cluster_iam_role_name
  policy_arn = data.aws_iam_policy.AmazonEKSVPCResourceController.arn
  depends_on = [
    module.eks
  ]
}