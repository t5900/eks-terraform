resource "random_string" "rds_password" {
  length           = 12
  special          = true
  override_special = "!#$&"

  keepers = {
    kepeer1 = var.app_name
    //keperr2 = var.something
  }
}

// Store Password in SSM Parameter Store
resource "aws_ssm_parameter" "rds_password" {
  name        = "/prod/postgresql"
  description = "Master Password for RDS"
  type        = "SecureString"
  value       = random_string.rds_password.result
  depends_on = [
    module.vpc-prod
  ]
}

// Get Password from SSM Parameter Store
data "aws_ssm_parameter" "my_rds_password" {
  name       = "/prod/postgresql"
  depends_on = [aws_ssm_parameter.rds_password]
}

module "rds" {
  depends_on = [
    module.vpc-prod
  ]
  source  = "terraform-aws-modules/rds/aws"
  version = "~> 3.0"

  identifier = var.app_name

  engine            = "postgres"
  engine_version    = "11.6"
  instance_class    = "db.t2.micro"
  allocated_storage = 5

  create_db_parameter_group = false

  name     = "django"
  username = "django"
  password = data.aws_ssm_parameter.my_rds_password.value
  port     = "5432"

  iam_database_authentication_enabled = false

  vpc_security_group_ids = tolist([module.security_group_rds.security_group_id])

  tags = {
    Owner       = "user"
    Environment = "dev"
  }

  # DB subnet group
  subnet_ids = module.vpc-prod.database_subnets
}

module "cache" {
  source                 = "./cache"
  cluster_id             = "${var.app_name}-cluster"
  subnet_groups          = module.vpc-prod.private_subnets
  cache_node_number      = 1
  security_group_ids_var = tolist([module.security_group_cache.security_group_id])
  depends_on = [
    module.vpc-prod
  ]
}
