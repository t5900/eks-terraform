output "cache_endpoint" {
    value = "${join(":", tolist([aws_elasticache_cluster.cache.cache_nodes.0.address, aws_elasticache_cluster.cache.cache_nodes.0.port]))}"
}

output "cache_address" {
    value = aws_elasticache_cluster.cache.cache_nodes.0.address
}

output "cache_port" {
    value = aws_elasticache_cluster.cache.cache_nodes.0.port
}