variable "region" {
  default = "us-east-1"
}

variable "app_name" {
  default = "django-twitter"
}