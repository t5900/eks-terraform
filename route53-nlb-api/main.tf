provider "aws" {
  region = var.region
}

locals {
  alias_ns = "api.bryidomain.tk"
  stage_name = "dev-env"
}

data "terraform_remote_state" "eks-app" {
  backend = "s3"
  config = {
    bucket = "eks-bryi"
    key    = "terraform.tfstate"
    region = "us-east-1"
  }
}

terraform {
  backend "s3" {
    bucket = "eks-bryi"
    key    = "route53-terraform.tfstate"
    region = "us-east-1"
  }
}


resource "aws_lb" "nlb" {
  load_balancer_type = "network"
}

resource "aws_api_gateway_vpc_link" "main" {
  name        = "vpc-link-${var.app_name}"
  target_arns = [aws_lb.nlb.arn]
  depends_on = [
    aws_lb.nlb
  ]
}

resource "aws_api_gateway_rest_api" "main" {
  name = "api-gateway-${var.app_name}"
  depends_on = [
    aws_api_gateway_vpc_link.main
  ]
}

resource "aws_api_gateway_resource" "main" {
  rest_api_id = aws_api_gateway_rest_api.main.id
  parent_id   = aws_api_gateway_rest_api.main.root_resource_id
  path_part   = "{proxy+}"
  depends_on = [
    aws_api_gateway_rest_api.main
  ]
}

resource "aws_api_gateway_method" "main" {
  rest_api_id   = aws_api_gateway_rest_api.main.id
  resource_id   = aws_api_gateway_resource.main.id
  http_method   = "ANY"
  authorization = "NONE"

  request_parameters = {
    "method.request.path.proxy" = true
  }
  depends_on = [
    aws_api_gateway_resource.main
  ]
}

resource "aws_api_gateway_integration" "main" {
  rest_api_id = aws_api_gateway_rest_api.main.id
  resource_id = aws_api_gateway_resource.main.id
  http_method = aws_api_gateway_method.main.http_method

  request_parameters = {
    "integration.request.path.proxy" = "method.request.path.proxy"
  }

  type                    = "HTTP_PROXY"
  uri                     = "http://${aws_lb.nlb.dns_name}/{proxy}"
  integration_http_method = "ANY"

  connection_type = "VPC_LINK"
  connection_id   = aws_api_gateway_vpc_link.main.id
  depends_on = [
    aws_api_gateway_method.main
  ]
}

resource "aws_api_gateway_deployment" "main" {
  rest_api_id = aws_api_gateway_rest_api.main.id
  stage_name  = local.stage_name
  depends_on  = [aws_api_gateway_integration.main]

  variables = {
    # just to trigger redeploy on resource changes
    resources = join(", ", [aws_api_gateway_resource.main.id])
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_zone" "primary_zone" {
  name = "bryidomain.tk"
}

resource "aws_api_gateway_domain_name" "example" {
  certificate_arn = "arn:aws:acm:us-east-1:249736386209:certificate/c702a64c-6ab4-4691-811e-6020afee12a5"
  domain_name     = local.alias_ns
  depends_on = [
    aws_api_gateway_deployment.main
  ]
}

resource "aws_api_gateway_base_path_mapping" "main" {
  api_id      = aws_api_gateway_rest_api.main.id
  stage_name  = local.stage_name
  domain_name = aws_api_gateway_domain_name.example.domain_name
  depends_on = [
    aws_api_gateway_domain_name.example
  ]
}

resource "aws_route53_record" "apigateway_alias" {
  zone_id = aws_route53_zone.primary_zone.zone_id
  name    = local.alias_ns
  type    = "A"

  alias {
    name                   = aws_api_gateway_domain_name.example.cloudfront_domain_name
    zone_id                = aws_api_gateway_domain_name.example.cloudfront_zone_id
    evaluate_target_health = true
  }

  depends_on = [
    aws_api_gateway_base_path_mapping.main
  ]
}