provider "kubectl" {
  host                   = data.aws_eks_cluster.eks.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.eks.token
  load_config_file       = false
}

locals {
  db_service     = "rds-service"
  cache_service  = "redis-service"
  cpu_request    = 50
  cpu_limit      = 500
  memory_request = 150
  memory_limit   = 200
}

data "kubectl_path_documents" "initial" {
  pattern = "./start-manifests/*.yaml"
}

resource "kubectl_manifest" "initial" {
  count     = length(data.kubectl_path_documents.initial.documents)
  yaml_body = element(data.kubectl_path_documents.initial.documents, count.index)
  depends_on = [
    module.eks,
    module.rds,
    module.cache,
  ]
}

resource "null_resource" "exec_kubectl" {
  provisioner "local-exec" {
    command = <<EOT
      cat kubeconfig_${var.app_name} > ~/.kube/config
      kubectl -n kube-system set env daemonset aws-node ENABLE_POD_ENI=true
      kubectl -n kube-system rollout status ds aws-node
    EOT
  }
  depends_on = [
    kubectl_manifest.initial,
    aws_iam_role_policy_attachment.cni-cluster-role-attachment
  ]
}

resource "kubectl_manifest" "db_internal_endpoint" {
  yaml_body = <<YAML
apiVersion: v1
kind: Service
metadata:
  labels:
    app: ${local.db_service}
  name: ${local.db_service}
spec:
  externalName: ${module.rds.db_instance_address}
  selector:
    app: ${local.db_service}
  type: ExternalName
status:
  loadBalancer: {}   
YAML
  depends_on = [
    null_resource.exec_kubectl
  ]
}

resource "kubectl_manifest" "cache_internal_endpoint" {
  yaml_body = <<YAML
apiVersion: v1
kind: Service
metadata:
  labels:
    app: ${local.cache_service}
  name: ${local.cache_service}
spec:
  externalName: ${module.cache.cache_address}
  selector:
    app: ${local.cache_service}
  type: ExternalName
status:
  loadBalancer: {}   
YAML
  depends_on = [
    module.eks,
    module.cache,
    null_resource.exec_kubectl
  ]
}

resource "kubectl_manifest" "db_secret" {
  yaml_body = <<YAML
apiVersion: v1
kind: Secret
metadata:
  name: ${var.app_name}-db
type: kubernetes.io/basic-auth
stringData:
  user: "${module.rds.db_instance_username}"
  password: "${data.aws_ssm_parameter.my_rds_password.value}"
YAML
  depends_on = [
    null_resource.exec_kubectl
  ]
}

resource "kubectl_manifest" "allow_rds_access" {
  yaml_body = <<YAML
apiVersion: vpcresources.k8s.aws/v1beta1
kind: SecurityGroupPolicy
metadata:
  name: ${var.app_name}-allow-rds-access
spec:
  podSelector:
    matchLabels:
      component: ${var.app_name}-api
      component: ${var.app_name}-worker
  securityGroups:
    groupIds:
      - ${aws_security_group.pod-sg.id}
YAML
  depends_on = [
    null_resource.exec_kubectl
  ]
}

resource "kubectl_manifest" "migrations_job" {
  yaml_body = <<YAML
apiVersion: batch/v1
kind: Job
metadata:
  name: ${var.app_name}-migrations
spec:
  backoffLimit: 0
  template:
    metadata:
      labels:
        component: ${var.app_name}-api
    spec:
      restartPolicy: Never
      containers:
        - name: ${var.app_name}-api
          image: ${var.docker_image}
          command: 
            - /bin/bash
            - -c
            - "python3 manage.py makemigrations && python3 manage.py makemigrations twit && python3 manage.py migrate"
          env:
          - name: SECRET_KEY
            value: "django-insecure-7-d54v=vy+d)!5p19p7mzq%w)8u&#53g1+!f!)nb^!25pw3o#3"
          - name: DEBUG
            value: "0"
          - name: DB_NAME
            value: "${module.rds.db_instance_name}"
          - name: DB_USER
            valueFrom:
              secretKeyRef:
                name: ${var.app_name}-db
                key: user
          - name: DB_PASSWORD
            valueFrom:
              secretKeyRef:
                name: ${var.app_name}-db
                key: password
          - name: DB_HOST
            value: "${local.db_service}"
          - name: DB_PORT
            value: "5432"
          - name: REDIS
            value: "redis://${local.cache_service}:${module.cache.cache_port}"  
YAML
  depends_on = [
    kubectl_manifest.allow_rds_access,
    kubectl_manifest.db_secret,
    kubectl_manifest.db_internal_endpoint,
    kubectl_manifest.cache_internal_endpoint
  ]
}

resource "kubectl_manifest" "api_deployment" {
  yaml_body = <<YAML
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ${var.app_name}-api
spec:
  replicas: 1
  selector:
    matchLabels:
      component: ${var.app_name}-api
  template:
    metadata:
      labels:
        component: ${var.app_name}-api
    spec:
      containers:
        - name: ${var.app_name}-api
          image: ${var.docker_image}
          command: ["./entrypoint.sh"]
          resources:
            requests:
              memory: "${local.memory_request}Mi"
              cpu: "${local.cpu_request}m"
            limits:
              memory: "${local.memory_limit}Mi"
              cpu: "${local.cpu_limit}m"
          ports:
            - containerPort: ${var.app_port}
          livenessProbe:
            httpGet:
              path: /health_check
              port: ${var.app_port}
            initialDelaySeconds: 30
            failureThreshold: 1
            periodSeconds: 10
          readinessProbe:
            httpGet:
              path: /health_check 
              port: ${var.app_port}
            initialDelaySeconds: 60
            periodSeconds: 10
          env:
          - name: SECRET_KEY
            value: "django-insecure-7-d54v=vy+d)!5p19p7mzq%w)8u&#53g1+!f!)nb^!25pw3o#3"
          - name: DEBUG
            value: "0"
          - name: DB_NAME
            value: "${module.rds.db_instance_name}"
          - name: DB_USER
            valueFrom:
              secretKeyRef:
                name: ${var.app_name}-db
                key: user
          - name: DB_PASSWORD
            valueFrom:
              secretKeyRef:
                name: ${var.app_name}-db
                key: password
          - name: DB_HOST
            value: "${local.db_service}"
          - name: DB_PORT
            value: "5432"
          - name: REDIS
            value: "redis://${local.cache_service}:${module.cache.cache_port}"
YAML
  depends_on = [
    kubectl_manifest.migrations_job,
    helm_release.cluster-autoscaler
  ]
}

resource "kubectl_manifest" "worker_deployment" {
  yaml_body = <<YAML
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ${var.app_name}-worker
spec:
  replicas: 11
  selector:
    matchLabels:
      component: ${var.app_name}-worker
  template:
    metadata:
      labels:
        component: ${var.app_name}-worker
    spec:
      containers:
        - name: ${var.app_name}-worker
          image: ${var.docker_image}
          command: ["celery", "--app", "twitter", "worker", "-l", "INFO"]
          resources:
            requests:
              memory: "${local.memory_request}Mi"
              cpu: "${local.cpu_request}m"
            limits:
              memory: "${local.memory_limit}Mi"
              cpu: "${local.cpu_limit}m"
          ports:
            - containerPort: ${var.app_port}
          env:
          - name: SECRET_KEY
            value: "django-insecure-7-d54v=vy+d)!5p19p7mzq%w)8u&#53g1+!f!)nb^!25pw3o#3"
          - name: DEBUG
            value: "0"
          - name: DB_NAME
            value: "${module.rds.db_instance_name}"
          - name: DB_USER
            valueFrom:
              secretKeyRef:
                name: ${var.app_name}-db
                key: user
          - name: DB_PASSWORD
            valueFrom:
              secretKeyRef:
                name: ${var.app_name}-db
                key: password
          - name: DB_HOST
            value: "${local.db_service}"
          - name: DB_PORT
            value: "5432"
          - name: REDIS
            value: "redis://${local.cache_service}:${module.cache.cache_port}"
YAML
  depends_on = [
    kubectl_manifest.api_deployment
  ]
}

resource "kubectl_manifest" "api_service" {
  yaml_body = <<YAML
apiVersion: v1
kind: Service
metadata:
  annotations:
    service.beta.kubernetes.io/aws-load-balancer-backend-protocol: tcp
    service.beta.kubernetes.io/aws-load-balancer-connection-idle-timeout: '60'
    service.beta.kubernetes.io/aws-load-balancer-cross-zone-load-balancing-enabled: 'true'
    service.beta.kubernetes.io/aws-load-balancer-type: nlb
    service.beta.kubernetes.io/aws-load-balancer-internal: 0.0.0.0/0
  name: ${var.app_name}-api-service
spec:
  type: LoadBalancer
  ports:
    - port: 80
      targetPort: ${var.app_port}
  selector:
    component: ${var.app_name}-api
YAML
  depends_on = [
    kubectl_manifest.api_deployment
  ]
}

resource "kubectl_manifest" "hpa_api" {
  yaml_body = <<YAML
apiVersion: autoscaling/v2beta2
kind: HorizontalPodAutoscaler
metadata:
  name: ${var.app_name}-hpa-api
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: ${var.app_name}-api
  minReplicas: 1
  maxReplicas: 10
  metrics:
  - type: Resource
    resource:
      name: cpu
      target:
        type: Utilization
        averageUtilization: 90
  - type: Resource
    resource:
      name: memory
      target:
        type: AverageValue
        averageValue: 400Mi 
YAML
  depends_on = [
    kubectl_manifest.api_deployment
  ]
}

resource "kubectl_manifest" "hpa_worker" {
  yaml_body = <<YAML
apiVersion: autoscaling/v2beta2
kind: HorizontalPodAutoscaler
metadata:
  name: ${var.app_name}-hpa-worker
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: ${var.app_name}-worker
  minReplicas: 1
  maxReplicas: 30
  metrics:
  - type: Resource
    resource:
      name: cpu
      target:
        type: Utilization
        averageUtilization: 90
  - type: Resource
    resource:
      name: memory
      target:
        type: AverageValue
        averageValue: 400Mi 
YAML
  depends_on = [
    kubectl_manifest.worker_deployment
  ]
}